﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
	Inherits System.Windows.Forms.Form

	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> _
	Protected Overrides Sub Dispose(ByVal disposing As Boolean)
		Try
			If disposing AndAlso components IsNot Nothing Then
				components.Dispose()
			End If
		Finally
			MyBase.Dispose(disposing)
		End Try
	End Sub

	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.  
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> _
	Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.nama = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.address = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.handphone = New System.Windows.Forms.TextBox()
        Me.male = New System.Windows.Forms.RadioButton()
        Me.female = New System.Windows.Forms.RadioButton()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.daftar = New System.Windows.Forms.Button()
        Me.date_pemeriksaan = New System.Windows.Forms.DateTimePicker()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Segoe UI", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point)
        Me.Label1.Location = New System.Drawing.Point(113, 50)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(402, 32)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Form Pendaftaran Pasien Poli Gigi"
        '
        'nama
        '
        Me.nama.Location = New System.Drawing.Point(113, 123)
        Me.nama.Name = "nama"
        Me.nama.Size = New System.Drawing.Size(100, 23)
        Me.nama.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(113, 105)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(76, 15)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Nama Pasien"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(113, 160)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(45, 15)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Alamat"
        '
        'address
        '
        Me.address.Location = New System.Drawing.Point(113, 178)
        Me.address.Name = "address"
        Me.address.Size = New System.Drawing.Size(100, 23)
        Me.address.TabIndex = 5
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(113, 220)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(42, 15)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "No HP"
        '
        'handphone
        '
        Me.handphone.Location = New System.Drawing.Point(113, 238)
        Me.handphone.Name = "handphone"
        Me.handphone.Size = New System.Drawing.Size(100, 23)
        Me.handphone.TabIndex = 7
        '
        'male
        '
        Me.male.AutoSize = True
        Me.male.Location = New System.Drawing.Point(322, 123)
        Me.male.Name = "male"
        Me.male.Size = New System.Drawing.Size(45, 19)
        Me.male.TabIndex = 9
        Me.male.TabStop = True
        Me.male.Text = "Pria"
        Me.male.UseVisualStyleBackColor = True
        '
        'female
        '
        Me.female.AutoSize = True
        Me.female.Location = New System.Drawing.Point(377, 123)
        Me.female.Name = "female"
        Me.female.Size = New System.Drawing.Size(62, 19)
        Me.female.TabIndex = 10
        Me.female.TabStop = True
        Me.female.Text = "Wanita"
        Me.female.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(322, 105)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(78, 15)
        Me.Label5.TabIndex = 11
        Me.Label5.Text = "Jenis Kelamin"
        '
        'daftar
        '
        Me.daftar.Location = New System.Drawing.Point(245, 292)
        Me.daftar.Name = "daftar"
        Me.daftar.Size = New System.Drawing.Size(75, 23)
        Me.daftar.TabIndex = 12
        Me.daftar.Text = "DAFTAR"
        Me.daftar.UseVisualStyleBackColor = True
        '
        'date_pemeriksaan
        '
        Me.date_pemeriksaan.Location = New System.Drawing.Point(315, 178)
        Me.date_pemeriksaan.Name = "date_pemeriksaan"
        Me.date_pemeriksaan.Size = New System.Drawing.Size(200, 23)
        Me.date_pemeriksaan.TabIndex = 13
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(315, 160)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(88, 15)
        Me.Label6.TabIndex = 14
        Me.Label6.Text = "Tanggal Periksa"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(47, 440)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(166, 23)
        Me.Button1.TabIndex = 15
        Me.Button1.Text = "tampilkan daftar pasien"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(651, 556)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.date_pemeriksaan)
        Me.Controls.Add(Me.daftar)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.female)
        Me.Controls.Add(Me.male)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.handphone)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.address)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.nama)
        Me.Controls.Add(Me.Label1)
        Me.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.Name = "Form1"
        Me.Text = "DAFTAR"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents nama As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents address As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents handphone As TextBox
    Friend WithEvents male As RadioButton
    Friend WithEvents female As RadioButton
    Friend WithEvents Label5 As Label
    Friend WithEvents daftar As Button
    Friend WithEvents date_pemeriksaan As DateTimePicker
    Friend WithEvents Label6 As Label
    Friend WithEvents Button1 As Button
End Class
